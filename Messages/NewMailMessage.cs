﻿using GalaSoft.MvvmLight.Messaging;
using MimeKit;
using ZalandoPromoCodeGenerator.Models;

namespace ZalandoPromoCodeGenerator.Messages
{
    public class NewMailMessage : GenericMessage<GmailMail>
    {
        public NewMailMessage(GmailMail content) : base(content)
        {
        }

        public NewMailMessage(object sender, GmailMail content) : base(sender, content)
        {
        }

        public NewMailMessage(object sender, object target, GmailMail content) : base(sender, target, content)
        {
        }
    }
}