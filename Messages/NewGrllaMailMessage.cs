﻿using System.Collections.Generic;
using GalaSoft.MvvmLight.Messaging;
using ZalandoPromoCodeGenerator.Models;

namespace ZalandoPromoCodeGenerator.Messages
{
    public class NewGrllaMailMessage : GenericMessage<List<GrllaMail>>
    {
        public NewGrllaMailMessage(List<GrllaMail> content) : base(content)
        {
        }

        public NewGrllaMailMessage(object sender, List<GrllaMail> content) : base(sender, content)
        {
        }

        public NewGrllaMailMessage(object sender, object target, List<GrllaMail> content) : base(sender, target, content)
        {
        }
    }
}