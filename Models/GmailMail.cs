﻿using MailKit;

namespace ZalandoPromoCodeGenerator.Models
{
    public class GmailMail
    {
        public UniqueId Uid { get; set; }
        public Envelope Envelope { get; set; }
    }
}