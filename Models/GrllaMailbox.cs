﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace ZalandoPromoCodeGenerator.Models
{
    public class GrllaMailbox
    {
        [JsonProperty("list")]
        public List<GrllaMail> List { get; set; }

        [JsonProperty("count")]
        public string _count { get; set; }

        [JsonIgnore]
        public int Count { get => int.Parse(_count); set => _count = value.ToString(); }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("ts")]
        public double Timestamp { get; set; }

        [JsonProperty("sid_token")]
        public string Token { get; set; }
    }
}
