﻿using System;
using Newtonsoft.Json;

namespace ZalandoPromoCodeGenerator.Models
{
    public class GrllaMail
    {
        [JsonProperty("mail_id")]
        public string Id { get; set; }

        [JsonProperty("mail_from")]
        public string From { get; set; }

        [JsonProperty("mail_subject")]
        public string Subject { get; set; }

        [JsonProperty("mail_excerpt")]
        public string Short { get; set; }

        [JsonProperty("mail_body")]
        public string Content { get; set; }

        [JsonProperty("content_type")]
        public string ContentType { get; set; }

        [JsonProperty("mail_timestamp")]
        public string Timestamp { get; set; }

        [JsonProperty("mail_read")]
        string _read { get; set; }

        [JsonIgnore]
        public bool Read { get => _read.Equals("1"); set => _read = (value ? "1" : "0"); }

        [JsonProperty("mail_date")]
        public DateTime Date { get; set; }
    }
}
