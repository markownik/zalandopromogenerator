﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using CefSharp;
using GalaSoft.MvvmLight.Messaging;
using HtmlAgilityPack;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using ZalandoPromoCodeGenerator.Interfaces;
using ZalandoPromoCodeGenerator.Messages;
using ZalandoPromoCodeGenerator.Models;

namespace ZalandoPromoCodeGenerator.Services
{
    public class GeneratorService : IGeneratorService
    {
        private readonly ILogger _logger;
        private readonly IGrllaMailClient _mailClient;
        private readonly Browser _browser;

        private int _stage;
        private TaskCompletionSource<string> _result;
        
        public GeneratorService(ILogger<GeneratorService> logger, IGrllaMailClient mailClient)
        {
            _logger = logger;
            _mailClient = mailClient;
            _browser = new Browser();
        }

        public async Task<string> GenerateCode()
        {
            Messenger.Default.Register<NewGrllaMailMessage>(this, MailMessageReceived);
            Random rnd = new Random();

            try
            {
                _logger.Log(LogLevel.Information, "Setting up mailbox...");
                _stage = 0;
                var grllaEmail = _mailClient.GetNewEmailAccount();
                _logger.Log(LogLevel.Information, $"Temp email: {grllaEmail}");
                _mailClient.StartRefreshingMailbox();

                _logger.Log(LogLevel.Information, "Subscribing to newsletter...");

                _browser.OpenUrl("https://www.zalando.pl/zalando-newsletter/");

                await _browser.Page.EvaluateScriptAsync(@"
                    var play = document.getElementById('email-input')
                    function findPos(obj)
                    {
                        var curleft = 0;
                        var curtop = 0;

                        if (obj.offsetParent)
                        {
                            do
                            {
                                curleft += obj.offsetLeft;
                                curtop += obj.offsetTop;
                            } while (obj = obj.offsetParent);

                            return { X: curleft,Y: curtop};
                        }
                    }
                    findPos(play)"
                    )
                    .ContinueWith(x =>
                    {
                        // 2. Continue with finding the coordinates and using MouseClick method 
                        // for pressing left mouse button down and releasing it at desired end position.
                        var responseForMouseClick = x.Result;

                        if (responseForMouseClick.Success && responseForMouseClick.Result != null)
                        {
                            var xy = responseForMouseClick.Result;
                            var json = JsonConvert.SerializeObject(xy).ToString();
                            var coordx = json.Substring(json.IndexOf(':') + 1, 3);
                            var coordy = json.Substring(json.LastIndexOf(':') + 1, 3);

                            _browser.MouseLeftDown(int.Parse(coordx) + 5, int.Parse(coordy) + 5);
                            _browser.MouseLeftUp(int.Parse(coordx) + 100, int.Parse(coordy) + 100);
                        }
                    });

                await _browser.Page.EvaluateScriptAsync($"document.getElementById('email-input').value = \"{grllaEmail}\";");
                await _browser.SimulateKeyboard();
                await _browser.Page.EvaluateScriptAsync($"document.getElementById('gender-{(rnd.Next(0, 1000) % 2 == 0 ? "1" : "2")}').click();");
                await _browser.Page.EvaluateScriptAsync($"document.evaluate(\"//span[contains(text(),'Zapisz mnie')]\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.parentNode.click();");

                _stage = 1;
                _logger.Log(LogLevel.Information, $"stage #{_stage}: Waiting for 1st e-mail... (1-2 minutes)");
                _result = new TaskCompletionSource<string>();
                var activationLink = await _result.Task;

                if (string.IsNullOrEmpty(activationLink))
                    throw new Exception("could not retrieve activation link");

                _logger.Log(LogLevel.Information, $"Got activation link: {activationLink}");

                _browser.OpenUrl(activationLink);

                _logger.Log(LogLevel.Information, "Everything is going great...");
                _result = new TaskCompletionSource<string>();

                _logger.Log(LogLevel.Information, $"stage #{_stage}: Waiting for 2nd e-mail... (1-2 minutes)");
                var promoCode = await _result.Task;

                if (string.IsNullOrEmpty(promoCode))
                    throw new Exception("could not get code from last email");

                _logger.Log(LogLevel.Information, $"Got promo code: {promoCode}");

                return promoCode;
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, $"OOOPS: {ex.Message}\n{ex.StackTrace}");
                return null;
            }
            finally
            {
                _mailClient.StopRefreshingMailbox();
            }
        }

        private void MailMessageReceived(NewGrllaMailMessage obj)
        {
            var email = obj.Content[0];
            Console.WriteLine($"Got new email, it's from <{email.From}>!");
            if (email.From.Contains("zalando.pl"))
            {
                email = _mailClient.FetchEmail(email.Id);
                ParseZalandoEmail(email.Content, email.ContentType);
            }
        }

        private void ParseZalandoEmail(string body, string type)
        {
            HtmlDocument doc;
            switch (_stage)
            {
                case 1: //process stage 1
                    if (body.Contains(@"Potwierdź adres e-mail"))
                    {
                        doc = new HtmlDocument();
                        doc.LoadHtml(body);
                        var hrefList = doc.DocumentNode.SelectNodes("//a")
                            .Select(p => p.GetAttributeValue("href", string.Empty))
                            .Where(a => a.Contains("https://www.zalando.pl/zalando-newsletter/confirmation"))
                            .ToList();
                        _stage = 2;
                        _result.SetResult(hrefList[0]);
                    }
                    break;
                case 2: //process stage 2
                    if (body.Contains(@"Na dobry początek"))
                    {
                        var regex = new Regex(@">([A-Z0-9]{10})<");
                        _result.SetResult(regex.Match(body)?.Value.Substring(1, 10));
                    }
                    break;
                default:
                    break;
            }
        }

        private static void DisplayBitmap(Task<Bitmap> task)
        {
            // Make a file to save it to (e.g. C:\Users\jan\Desktop\CefSharp screenshot.png)
            var screenshotPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), "CefSharp screenshot" + DateTime.Now.Ticks + ".png");

            Console.WriteLine();
            Console.WriteLine("Screenshot ready. Saving to {0}", screenshotPath);

            var bitmap = task.Result;

            // Save the Bitmap to the path.
            // The image type is auto-detected via the ".png" extension.
            bitmap.Save(screenshotPath);

            // We no longer need the Bitmap.
            // Dispose it to avoid keeping the memory alive.  Especially important in 32-bit applications.
            bitmap.Dispose();

            Console.WriteLine("Screenshot saved. Launching your default image viewer...");

            // Tell Windows to launch the saved image.
            Process.Start(new ProcessStartInfo(screenshotPath)
            {
                // UseShellExecute is false by default on .NET Core.
                UseShellExecute = true
            });

            Console.WriteLine("Image viewer launched.  Press any key to exit.");
        }
    }
}