﻿using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Ioc;
using GalaSoft.MvvmLight.Messaging;
using MailKit;
using MailKit.Net.Imap;
using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.Extensions.Logging;
using MimeKit;
using ZalandoPromoCodeGenerator.Interfaces;
using ZalandoPromoCodeGenerator.Messages;
using ZalandoPromoCodeGenerator.Models;


namespace ZalandoPromoCodeGenerator.Services
{
    public class GmailClient : IMailClient
    {
        private readonly ILogger _logger;
        private ImapClient _imap = new ImapClient();
        private volatile bool _refreshBox;
        private Thread RefreshThread { get; set; }

        public GmailClient(ILogger<GmailClient> logger)
        {
            _logger = logger;
        }

        public void StartRefreshingMailbox()
        {
            StopRefreshingMailbox();

            RefreshThread = new Thread(async () => {
                _refreshBox = true;
                
                while (_refreshBox)
                {
                    CheckNewMail();
                    await Task.Delay(5000);
                }

                if (_imap.IsConnected)
                    await _imap.DisconnectAsync(true);
            });
            RefreshThread.Start();
        }

        public void StopRefreshingMailbox()
        {
            _refreshBox = false;
            RefreshThread?.Join(5500);
        }

        private async void CheckNewMail()
        {
            if (!_imap.IsConnected)
                await _imap.ConnectAsync("imap.gmail.com", 993, true);
            if (!_imap.IsAuthenticated)
                await _imap.AuthenticateAsync(Environment.GetEnvironmentVariable("GMAIL_ACCOUNT"), Environment.GetEnvironmentVariable("GMAIL_PASSWORD"));

            var inbox = _imap.Inbox;
            if (!inbox.IsOpen)
                await inbox.OpenAsync(FolderAccess.ReadWrite);

            foreach (var msg in inbox.Fetch(0, -1, MessageSummaryItems.UniqueId | MessageSummaryItems.Flags | MessageSummaryItems.Envelope))
            {
                _logger.Log(LogLevel.Debug, $"found email...  {msg.UniqueId}");
                var title = msg.Envelope?.Subject?.ToLower();
                if (!string.IsNullOrEmpty(title) && !title.Contains("proszę") && !title.Contains("kod"))
                {
                    _logger.Log(LogLevel.Debug, $"spam, deleted");
                    await inbox.AddFlagsAsync(msg.UniqueId, MessageFlags.Deleted, true);
                    continue;
                }

                if ((msg.Flags & MessageFlags.Seen) == 0)
                {
                    _logger.Log(LogLevel.Debug, $"new email!");
                    if(_imap.IsConnected && inbox.IsOpen)
                        await inbox.AddFlagsAsync(msg.UniqueId, MessageFlags.Deleted, true);
                    Messenger.Default.Send(new NewMailMessage(new GmailMail()
                    {
                        Uid = msg.UniqueId,
                        Envelope = msg.Envelope
                    }));
                    StopRefreshingMailbox();
                }
            }

            if(inbox.IsOpen)
                await inbox.CloseAsync(true);
            if (_imap.IsConnected)
                await _imap.DisconnectAsync(true);
        }

        public async void DeleteMessage(UniqueId messageId)
        {
            if(!_imap.IsConnected)
                await _imap.ConnectAsync("imap.gmail.com", 993, true);
            if(!_imap.IsAuthenticated)
                await _imap.AuthenticateAsync(Environment.GetEnvironmentVariable("GMAIL_ACCOUNT"), Environment.GetEnvironmentVariable("GMAIL_PASSWORD"));

            var inbox = _imap.Inbox;
            if(!inbox.IsOpen)
                await inbox.OpenAsync(FolderAccess.ReadWrite);
            await inbox.AddFlagsAsync(messageId, MessageFlags.Deleted, true);

            if (_imap.IsConnected)
                await _imap.DisconnectAsync(true);
        }

        public async void SendMessage(string subject, string body, InternetAddress recipient, string replyId = null)
        {
            var message = new MimeMessage();
            message.Subject = subject;
            message.Body = new TextPart("plain") { Text = body };
            message.From.Add(new MailboxAddress("Zalando Promo Generator", "zalando.promo.bot@gmail.com"));
            message.To.Add(recipient);
            if (replyId != null)
                message.InReplyTo = replyId;

            using var smtp = new SmtpClient();
            await smtp.ConnectAsync("smtp.gmail.com", 465, true);
            await smtp.AuthenticateAsync(Environment.GetEnvironmentVariable("GMAIL_ACCOUNT"), Environment.GetEnvironmentVariable("GMAIL_PASSWORD"));
            await smtp.SendAsync(message);
            await smtp.DisconnectAsync(true);
            StartRefreshingMailbox();
        }
    }
}