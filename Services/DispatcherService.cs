﻿using System.Linq;
using GalaSoft.MvvmLight.Messaging;
using Microsoft.Extensions.Logging;
using ZalandoPromoCodeGenerator.Interfaces;
using ZalandoPromoCodeGenerator.Messages;

namespace ZalandoPromoCodeGenerator.Services
{
    public class DispatcherService : IDispatcherService
    {
        private readonly ILogger _logger;
        private readonly IMailClient _gmail;
        private readonly IGeneratorService _generator;

        public DispatcherService(ILogger<DispatcherService> logger, IMailClient gmail, IGeneratorService generator)
        {
            _logger = logger;
            _gmail = gmail;
            _generator = generator;
            Messenger.Default.Register<NewMailMessage>(this, NewMailDetected);
            _gmail.StartRefreshingMailbox();
        }

        private async void NewMailDetected(NewMailMessage msg)
        {
            _logger.Log(LogLevel.Debug, $"dispatcher got new mail: {msg.Content.Uid} from: {msg.Content.Envelope.From}");
            var code = await _generator.GenerateCode();
            _gmail.SendMessage("Kod Rabatowy Zalando 10%", $"Twój kod: {code}", msg.Content.Envelope.ReplyTo[0], msg.Content.Envelope.MessageId);
            _gmail.DeleteMessage(msg.Content.Uid);
        }
    }
}