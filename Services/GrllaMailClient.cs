﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using GalaSoft.MvvmLight.Messaging;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using ZalandoPromoCodeGenerator.Interfaces;
using ZalandoPromoCodeGenerator.Messages;
using ZalandoPromoCodeGenerator.Models;

namespace ZalandoPromoCodeGenerator.Services
{
    public class GrllaMailClient : IGrllaMailClient
    {
        private readonly ILogger _logger;
        private Random _rnd = new Random();
        private CookieContainer _cookies;
        private string _sidToken = string.Empty;
        private volatile bool _refreshBox;
        
        public IWebProxy Proxy { get; }
        public List<GrllaMail> Emails { get; private set; }
        public Thread RefreshThread { get; private set; }

        public GrllaMailClient(ILogger<GrllaMailClient> logger)
        {
            _logger = logger;
            Proxy = WebRequest.GetSystemWebProxy();
        }

        public string GetNewEmailAccount()
        {
            _cookies = new CookieContainer();
            _sidToken = string.Empty;
            Emails = new List<GrllaMail>();

            var response = Send("get_email_address");
            var email = (string)response?.email_addr;
            var alias = (string)response?.alias;

            return $"{alias}@{email?.Split('@')?[1]}";
        }

        public void StartRefreshingMailbox()
        {
            StopRefreshingMailbox();

            RefreshThread = new Thread(() => {
                _refreshBox = true;
                ReloadInbox();
                while (_refreshBox)
                {
                    Thread.Sleep(1500);
                    try
                    {
                        CheckNewMail();
                    }
                    catch (Exception e)
                    {
                        _logger.Log(LogLevel.Error, e.Message);
                    }
                }
            });
            RefreshThread.Start();
        }

        public void StopRefreshingMailbox()
        {
            _refreshBox = false;
            RefreshThread?.Join(2000);
        }

        private bool CheckNewMail()
        {
            var response = Send<GrllaMailbox>("check_email", new Dictionary<string, object> { {"seq", 0} });

            if (response == null)
                return false;

            if (response.Count > 0)
            {
                Messenger.Default.Send(new NewGrllaMailMessage(response.List.GetRange(response.List.Count - (1 + response.Count), response.Count)));
                return true;
            }

            return false;
        }

        public void ReloadInbox()
        {
            var response = Send<GrllaMailbox>("get_email_list", new Dictionary<string, object> { { "offset", 0 } });

            if (response == null)
                return;

            Emails = response.List;
        }

        public GrllaMail FetchEmail(string id)
        {
            return Send<GrllaMail>("fetch_email", new Dictionary<string, object> { { "email_id", id } });
        }

        private TOut Send<TOut>(string function, Dictionary<string, object> arguments = null)
        {
            var reqParams = new Dictionary<string, string>() { ["f"] = function };

            if (arguments?.Count > 0)
                foreach (var arg in arguments)
                    reqParams.Add(arg.Key, arg.Value.ToString());

            if (!string.IsNullOrEmpty(_sidToken))
                reqParams.Add("sid_token", _sidToken);

            var args = string.Join('&', reqParams.Select(el => $"{el.Key}={el.Value}"));

            var request = new HttpRequestMessage(HttpMethod.Get, $"https://api.guerrillamail.com/ajax.php?{args}");

            //request.Content = new FormUrlEncodedContent(reqParams);

            try
            {
                HttpResponseMessage response;
                using (var handler = new HttpClientHandler() { CookieContainer = _cookies, Proxy = Proxy })
                using (var httpClient = new HttpClient(handler))
                {
                    response = httpClient.SendAsync(request).Result;
                }

                response.EnsureSuccessStatusCode();
                var content = response.Content.ReadAsStringAsync().Result;
                response.Dispose();

                dynamic result = JsonConvert.DeserializeObject<ExpandoObject>(content);
                
                if (string.IsNullOrEmpty(_sidToken))
                    _sidToken = (string)result?.sid_token;

                result = JsonConvert.DeserializeObject<TOut>(content);

                return result;
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, $"OOOPS: {ex.Message}\n{ex.StackTrace}");
            }

            return default;
        }

        private dynamic Send(string function, Dictionary<string, object> arguments = null)
        {
            return Send<dynamic>(function, arguments);
        }
    }
}
