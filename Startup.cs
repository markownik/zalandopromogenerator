using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;
using ZalandoPromoCodeGenerator.Interfaces;
using ZalandoPromoCodeGenerator.Services;
using ZalandoPromoGenerator;

namespace ZalandoPromoCodeGenerator
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        private readonly IWebHostEnvironment _env;

        public Startup(IConfiguration configuration, IWebHostEnvironment env)
        {
            Configuration = configuration;
            _env = env;

            if(env.IsDevelopment())
                Log.Logger = new LoggerConfiguration()
                    .MinimumLevel.Debug()
                    .Enrich.FromLogContext()
                    .WriteTo.Debug()
                    .CreateLogger();
        }
        
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddLogging(loggingBuilder =>
                loggingBuilder.AddSerilog(dispose: true));

            services.Add(ServiceDescriptor.Transient<IGrllaMailClient, GrllaMailClient>());
            services.Add(ServiceDescriptor.Transient<IGeneratorService, GeneratorService>());
            services.Add(ServiceDescriptor.Singleton<IDispatcherService, DispatcherService>());
            services.Add(ServiceDescriptor.Singleton<IMailClient, GmailClient>());

            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IGeneratorService generator, ILogger<Startup> logger)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            //Task.Factory.StartNew(async () =>
            //{
            //    var code = await generator.GenerateCode();
            //    logger.LogDebug($"code: {code}");
            //});
        }
    }
}
