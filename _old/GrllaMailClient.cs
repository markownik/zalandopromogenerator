﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Net;
using System.Net.Http;
using System.Threading;
using ZalandoPromoGenerator.Models;

namespace ZalandoPromoGenerator
{
    public class GrllaMailClient
    {
        private Random _rnd = new Random();
        private CookieContainer _cookies;
        private string _sidToken = string.Empty;
        private volatile bool _refreshBox;
        
        public IWebProxy Proxy { get; }
        public List<Mail> Emails { get; private set; }
        public Thread RefreshThread { get; private set; }
        public EventHandler<MailReceivedEventArgs> NewMailReceived { get; set; }

        public GrllaMailClient()
        {
            Proxy = WebRequest.GetSystemWebProxy();
        }

        public string GetNewEmailAccount()
        {
            _cookies = new CookieContainer();
            _sidToken = string.Empty;
            Emails = new List<Mail>();

            var response = Send("get_email_address");
            var email = (string)response?.email_addr;
            var alias = (string)response?.alias;

            return $"{alias}@{email?.Split('@')?[1]}";
        }

        public void StartRefreshingMailbox()
        {
            StopRefreshingMailbox();

            RefreshThread = new Thread(() => {
                _refreshBox = true;
                ReloadInbox();
                while (_refreshBox)
                {
                    Thread.Sleep(1500);
                    CheckNewMail();
                }
            });
            RefreshThread.Start();
        }

        public void StopRefreshingMailbox()
        {
            _refreshBox = false;
            RefreshThread?.Join(2000);
        }

        private bool CheckNewMail()
        {
            var response = Send<Mailbox>("check_email", new Dictionary<string, object> { {"seq", 0} });

            if (response == null)
                return false;

            if (response.Count > 0)
            {
                NewMailReceived?.Invoke(this, new MailReceivedEventArgs(response.List.GetRange(response.List.Count - (1 + response.Count), response.Count)));
                return true;
            }

            return false;
        }

        public void ReloadInbox()
        {
            var response = Send<Mailbox>("get_email_list", new Dictionary<string, object> { { "offset", 0 } });

            if (response == null)
                return;

            Emails = response.List;
        }

        public Mail FetchEmail(string id)
        {
            return Send<Mail>("fetch_email", new Dictionary<string, object> { { "email_id", id } });
        }

        private TOut Send<TOut>(string function, Dictionary<string, object> arguments = null)
        {
            var reqParams = new Dictionary<string, string>() { ["f"] = function };

            if (arguments?.Count > 0)
                foreach (var arg in arguments)
                    reqParams.Add(arg.Key, arg.Value.ToString());

            if (!string.IsNullOrEmpty(_sidToken))
                reqParams.Add("sid_token", _sidToken);

            var request = new HttpRequestMessage(HttpMethod.Post, $"https://api.guerrillamail.com/ajax.php");

            request.Content = new FormUrlEncodedContent(reqParams);

            try
            {
                HttpResponseMessage response;
                using (var handler = new HttpClientHandler() { CookieContainer = _cookies, Proxy = Proxy })
                using (var httpClient = new HttpClient(handler))
                {
                    response = httpClient.SendAsync(request).Result;
                }

                response.EnsureSuccessStatusCode();
                var content = response.Content.ReadAsStringAsync().Result;
                response.Dispose();

                dynamic result = JsonConvert.DeserializeObject<ExpandoObject>(content);
                
                if (string.IsNullOrEmpty(_sidToken))
                    _sidToken = (string)result?.sid_token;

                result = JsonConvert.DeserializeObject<TOut>(content);

                return result;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"OOOPS: {ex.Message}\n{ex.StackTrace}");
            }

            return default;
        }

        private dynamic Send(string function, Dictionary<string, object> arguments = null)
        {
            return Send<dynamic>(function, arguments);
        }

        public class MailReceivedEventArgs : EventArgs
        {
            public List<Mail> NewMail { get; }

            public MailReceivedEventArgs(List<Mail> mail) : base()
            {
                NewMail = mail;
            }
        }
    }
}
