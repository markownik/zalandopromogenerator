﻿using System;
using System.Net;
using System.Net.Http;
using Newtonsoft.Json;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Xml;
using HtmlAgilityPack;
using System.Web;
using System.Dynamic;
//using SimpleBrowser;

namespace ZalandoPromoGenerator
{
    class Program
    {
        const string UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0";
        static readonly  Uri CookiesUri = new Uri("https://zalando.pl/");
        static readonly Uri BackendUri = new Uri("https://www.zalando.pl/");
        static GrllaMailClient _mailClient;
        static int _stage;
        static string _activationLink = string.Empty;
        static string _promoCode = string.Empty;

        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to zalando.pl promo code generator! :)");
            _mailClient = new GrllaMailClient();
            Random rnd = new Random();
            IWebProxy proxy = WebRequest.GetSystemWebProxy();
            if (string.IsNullOrEmpty(proxy.GetProxy(BackendUri)?.ToString()))
                proxy = null;

            try
            {
                Console.WriteLine("Setting up mailbox...");
                _stage = 0;
                var grrlaEmail = _mailClient.GetNewEmailAccount();
                _mailClient.NewMailReceived += MailReceivedEventHandler;
                _mailClient.StartRefreshingMailbox();

                
                var cookieContainer = new CookieContainer();
                using (var handler = new HttpClientHandler() { CookieContainer = cookieContainer, Proxy = proxy })
                using (var httpClient = new HttpClient(handler) { BaseAddress = BackendUri })
                {
                    httpClient.DefaultRequestHeaders.Add("User-Agent", UserAgent);
                    httpClient.DefaultRequestHeaders.Add("Host", "www.zalando.pl");
                    httpClient.DefaultRequestHeaders.Add("Accept-Encoding", "gzip, deflate, br");
                    httpClient.DefaultRequestHeaders.Add("Connection", "keep-alive");

                    Console.WriteLine("Getting required cookies...");

                    var request = new HttpRequestMessage(HttpMethod.Get, "zalando-newsletter");
                    request.Headers.Add("Accept", "*/*");
                    var response = httpClient.SendAsync(request).Result;
                    response.EnsureSuccessStatusCode();

                    var xsrfToken = cookieContainer.GetCookies(CookiesUri).Single(c => c.Name.Equals("frsx")).Value;

                    Task.Delay(500 + rnd.Next(500, 1000));

                    Console.WriteLine("Subscribing to newsletter...");

                    var requestBody = new
                    {
                        email = grrlaEmail,
                        gender = rnd.Next(0,1000) % 2 == 0 ? "MALE" : "FEMALE",
                        consent_status = "APPROVED",
                        consentReferrer = "nl_subscription_page",
                        topics = new []
                        {
                            new {
                                enabled = true,
                                key = "survey",
                            },
                            new {
                                enabled = true,
                                key = "recommendations",
                            },
                            new {
                                enabled = true,
                                key = "fashion_fix",
                            },
                            new {
                                enabled = true,
                                key = "offers_sales",
                            },
                            new {
                                enabled = true,
                                key = "follow_brand",
                            },
                            new {
                                enabled = true,
                                key = "item_alerts",
                            },
                            new {
                                enabled = true,
                                key = "subscription_confirmations",
                            },
                        } 
                    };

                    request = new HttpRequestMessage(HttpMethod.Put, "api/consent-service/consents")
                    {
                        Content = new StringContent(JsonConvert.SerializeObject(requestBody), Encoding.UTF8, "application/json")
                    };
                    request.Headers.Add("x-xsrf-token", xsrfToken);
                    response = httpClient.SendAsync(request).Result;
                    response.EnsureSuccessStatusCode();

                    Console.WriteLine("Waiting for 1st e-mail... (1-2 minutes)");
                    _mailClient.RefreshThread.Join();

                    if(string.IsNullOrEmpty(_activationLink))
                    {
                        Console.WriteLine("OOOPS: Could not retrieve activation link :(");
                        return;
                    }

                    Console.WriteLine("Got activation link :)");

                    Uri actUri = new Uri(_activationLink);
                    string uuid = HttpUtility.ParseQueryString(actUri.Query).Get("cr");

                    request = new HttpRequestMessage(HttpMethod.Get, actUri);
                    response = httpClient.SendAsync(request).Result;
                    response.EnsureSuccessStatusCode();

                    xsrfToken = cookieContainer.GetCookies(CookiesUri).Single(c => c.Name.Equals("frsx")).Value;

                    request = new HttpRequestMessage(HttpMethod.Patch, $"api/consent-service/consent-references/{uuid}/verify");
                    request.Headers.Add("x-xsrf-token", xsrfToken);
                    response = httpClient.SendAsync(request).Result;
                    response.EnsureSuccessStatusCode();

                    dynamic result = JsonConvert.DeserializeObject<ExpandoObject>(response.Content.ReadAsStringAsync().Result);
                    if ((int)result.status != 200 || !((string)result.email).Equals(grrlaEmail))
                    {
                        Console.WriteLine("Something is not ok :(");
                        return;
                    }
                    Console.WriteLine("Everything is going great...");

                    _mailClient.StartRefreshingMailbox();
                    Console.WriteLine("Waiting for 2nd e-mail... (1-2 minutes)");
                    _mailClient.RefreshThread.Join();

                    if(string.IsNullOrEmpty(_promoCode))
                    {
                        Console.WriteLine("Problem retrieving promo code from the last email :(");
                        return;
                    }

                    Console.WriteLine("\n###############################################\n#");
                    Console.WriteLine($"#\tYOUR CODE (PYSIOWY KOD): {_promoCode}\n#");
                    Console.WriteLine("###############################################");

                    Console.ReadKey();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"OOOPS: {ex.Message}\n{ex.StackTrace}");
            }
        }

        private static void MailReceivedEventHandler(object sender, GrllaMailClient.MailReceivedEventArgs e)
        {
            var email = e.NewMail[0];
            Console.WriteLine($"Got new email, it's from <{email.From}>!");
            if(email.From.Contains("zalando.pl"))
            {
                _stage++;
                _mailClient.StopRefreshingMailbox();
                email = _mailClient.FetchEmail(email.Id);
                ParseZalandoEmail(email.Content, email.ContentType);
            }   
        }

        private static void ParseZalandoEmail(string body, string type)
        {
            HtmlDocument doc;
            switch (_stage)
            {
                case 1: //process stage 1
                    doc = new HtmlDocument();
                    doc.LoadHtml(body);
                    var hrefList = doc.DocumentNode.SelectNodes("//a")
                                  .Select(p => p.GetAttributeValue("href", string.Empty))
                                  .Where(a => a.Contains("https://www.zalando.pl/zalando-newsletter/confirmation"))
                                  .ToList();
                    _activationLink = hrefList[0];
                    break;
                case 2: //process stage 2
                    var regex = new Regex(@">([A-Z0-9]{10})<");
                    _promoCode = regex.Match(body)?.Value.Substring(1,10);
                    break;
                default:
                    break;
            }
        }
    }
}
