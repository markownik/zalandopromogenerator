﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace ZalandoPromoGenerator.Models
{
    public class Mailbox
    {
        [JsonProperty("list")]
        public List<Mail> List { get; set; }

        [JsonProperty("count")]
        public string _count { get; set; }

        [JsonIgnore]
        public int Count { get => int.Parse(_count); set => _count = value.ToString(); }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("ts")]
        public double Timestamp { get; set; }

        [JsonProperty("sid_token")]
        public string Token { get; set; }
    }
}
