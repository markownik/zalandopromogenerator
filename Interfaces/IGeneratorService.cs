﻿using System.Threading.Tasks;

namespace ZalandoPromoCodeGenerator.Interfaces
{
    public interface IGeneratorService
    {
        public Task<string> GenerateCode();
    }
}