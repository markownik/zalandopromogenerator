﻿using System.Net.Mail;
using MailKit;
using MimeKit;

namespace ZalandoPromoCodeGenerator.Interfaces
{
    public interface IMailClient
    {
        void StartRefreshingMailbox();

        void StopRefreshingMailbox();

        void SendMessage(string subject, string body, InternetAddress recipient, string replyId = null);

        void DeleteMessage(UniqueId messageId);
    }
}