﻿using ZalandoPromoCodeGenerator.Models;

namespace ZalandoPromoCodeGenerator.Interfaces
{
    public interface IGrllaMailClient
    {
        public string GetNewEmailAccount();
        public void StartRefreshingMailbox();
        public void StopRefreshingMailbox();
        public GrllaMail FetchEmail(string id);
    }
}